package br.com.seatelecom.app.ui.call.current.tab

import android.annotation.SuppressLint
import android.icu.util.Calendar
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import androidx.navigation.fragment.findNavController
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import br.com.seatelecom.app.R
import br.com.seatelecom.app.databinding.StartCallPopUI
import br.com.seatelecom.app.databinding.TabCallUI
import br.com.seatelecom.app.ui.call.current.CallViewModel
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import java.text.SimpleDateFormat


class TabFragment : Fragment() {

    private lateinit var viewModel: CallViewModel
    private lateinit var bind: TabCallUI
    private lateinit var bind2: StartCallPopUI


    @SuppressLint("ResourceAsColor")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProvider(requireActivity())[CallViewModel::class.java]

        val actionBar = (requireActivity() as AppCompatActivity).supportActionBar!!
        actionBar.elevation = 0F
        actionBar.show()
        actionBar.title = getString(R.string.title_attendance_2)

        bind = TabCallUI.inflate(inflater)
        viewModel.call.observe(viewLifecycleOwner, Observer {

            bind.tabProtocol.text = it.attendance!!.protocol
            bind.tabClient.text = it.attendance!!.nameClient

            if ((it.repair!!.comments.size > 0) && (it.materialList.size > 0)) {
                bind.concluir.isEnabled = true
            }else{
                bind.concluir.isEnabled = false

            }

        })
        bind.infosPop.setOnClickListener {
            val mDialog: BottomSheetDialog =
                BottomSheetDialog(this.requireContext(), R.style.BottomSheetDialogTheme)
            bind2 = StartCallPopUI.inflate(inflater)

            viewModel.call.observe(viewLifecycleOwner, Observer {
                bind2.protocol.text = it.attendance!!.protocol
                bind2.clientName.text = it.attendance!!.nameClient
                bind2.description.text = it.attendance!!.description
                bind2.operator.text = it.attendance!!.operator
                bind2.hour.text = getDate(it.attendance!!.create, "dd/MM/yyyy - hh:mm:ss")

            })
            mDialog.setContentView(bind2.root)
            mDialog.show()
        }


        bind.concluir.setOnClickListener {
            viewModel.setConclusionDate()
            viewModel.endRepair()
            viewModel.concludedSelectMaterial()

            findNavController().navigate(R.id.action_tabFragment_to_finalFragment)

        }

        addFragment(bind.root)
        return bind.root
    }

    private fun getDate(milliSeconds: Long, dateFormat: String): CharSequence? {
        // Create a DateFormatter object for displaying date in specified format.
        // Create a DateFormatter object for displaying date in specified format.
        val formatter = SimpleDateFormat(dateFormat)

        // Create a calendar object that will convert the date and time value in milliseconds to date.

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        val calendar: Calendar = Calendar.getInstance()
        calendar.setTimeInMillis(milliSeconds)
        return formatter.format(calendar.getTime())
    }

    private fun openDialog() {


    }

    private fun addFragment(view: View) {

        val tabLayout: TabLayout = view.findViewById(R.id.tabLayoutCurrent)
        val viewPager: ViewPager2 = view.findViewById(R.id.viewPager)

        val adapter =
            ViewPagerAdapter(
                childFragmentManager,
                lifecycle
            )

        viewPager.adapter = adapter
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when (position) {
                0 -> {

                    tab.text = "COMENTÁRIOS"

                }
                1 -> {

                    tab.text = "MATERIAIS"

                }
            }
        }.attach()


    }


}