package br.com.seatelecom.app.model.call

import com.google.gson.*
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import org.bson.types.ObjectId
import java.util.*

open class Call: RealmObject() {
    @PrimaryKey
    @SerializedName("_id")
    var id: ObjectId = ObjectId()
    var companyId: String = ""
    var operatorId: String = ""
    var end: Long = 0
    var create = Calendar.getInstance().time.time
    var status: String = "new"
    var current = true
    var createSync = true
    var updateFile = false
    var location: RealmList<LocationSeaPoint> = RealmList()
    var attendance: Attendance? = Attendance()
    var beforeRepair: BeforeRepair? = null
    var repair: Repair? = Repair()
    var materialList: RealmList<MaterialSelected> = RealmList()



    fun getJson(): JsonObject {
        val json = JsonObject()

        json.addProperty("companyId", companyId)
        json.addProperty("operatorId", operatorId)
        json.addProperty("end", end)
        json.addProperty("status", status)
        json.addProperty("_id", id.toString())

        var array = JsonArray()
        for (aux in location) {
            array.add(aux.getJson())
        }

        json.add("location", array)

        json.add("start", attendance?.getJson())
        if (beforeRepair == null) {
            json.add("beforeRepair", BeforeRepair().getJson())
        } else {
            json.add("beforeRepair", beforeRepair?.getJson())
        }
        json.add("repair", repair?.getJson())

        array = JsonArray()
        for (material in materialList) {
            array.add(material?.getJson())
        }
        json.add("materialList", array)

        return json
    }


}