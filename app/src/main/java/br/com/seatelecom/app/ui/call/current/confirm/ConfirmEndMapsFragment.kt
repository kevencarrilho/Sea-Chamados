package br.com.seatelecom.app.ui.call.current.confirm

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import br.com.seatelecom.app.R
import br.com.seatelecom.app.databinding.ConfirmEndMapUI
import br.com.seatelecom.app.model.call.Call
import br.com.seatelecom.app.model.call.LocationSeaPoint
import br.com.seatelecom.app.ui.call.current.CallViewModel
import com.google.android.material.transition.MaterialSharedAxis
import org.osmdroid.api.IMapController
import org.osmdroid.config.Configuration
import org.osmdroid.util.BoundingBox
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.Polyline
import android.widget.Toast

import org.osmdroid.views.MapView


class ConfirmEndMapsFragment : Fragment() {


    private lateinit var bind: ConfirmEndMapUI
    private lateinit var viewModel: CallViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity())[CallViewModel::class.java]
        val forward = MaterialSharedAxis(MaterialSharedAxis.X, true)
        enterTransition = forward

        val backward = MaterialSharedAxis(MaterialSharedAxis.X, false)
        returnTransition = backward

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bind = br.com.seatelecom.app.databinding.ConfirmEndMapUI.inflate(inflater)

        val ctx: Context = requireActivity().getApplicationContext()
        Configuration.getInstance().load(ctx, requireActivity().getPreferences(0))

        bind.map.zoomController.onDetach()
        bind.map.setMultiTouchControls(true)
        val mapController: IMapController = bind.map.getController()


        viewModel.call.observe(viewLifecycleOwner, Observer {



            var geoPoints: ArrayList<GeoPoint> = ArrayList()

            for (loc in it.location) {
                geoPoints.add(GeoPoint(loc.local[0]!!, loc.local[1]!!, loc.local[2]!!))
            }


            val line = Polyline() //see note below!
            line.outlinePaint.strokeWidth = 5F
            line.outlinePaint.setColor(requireActivity().getColor(R.color.colorPrimaryDark))
            line.setPoints(geoPoints)
//            line.setOnClickListener { poly, mapView, eventPos ->
//                Toast.makeText(
//                    mapView.context,
//                    "Esta linha é seu trajeto",
//                    Toast.LENGTH_LONG
//                ).show()
//                false
//            }

            bind.map.overlayManager.add(line)

            val geoPointList = arrayListOf<GeoPoint>()

            val defPoint = arrayOf(
                it.beforeRepair!!.defectLocation!!.local[0]!!,
                it.beforeRepair!!.defectLocation!!.local[1]!!,
                it.beforeRepair!!.defectLocation!!.local[2]!!,
            )
            createMarker(defPoint, geoPointList, mapController)

        })

        return bind.root
    }

    fun createMarker(
        location: Array<Double>,
        geoPointList: ArrayList<GeoPoint>,
        mapController: IMapController
    ) {
        val pt = Marker(bind.map)

        val geoPoint = GeoPoint(
            location[0]!!,
            location[1]!!,
            location[2]!!
        )
        geoPointList.add(geoPoint)
        pt.position = geoPoint
        pt.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
        pt.title = "Local da falha"
        pt.icon = ContextCompat.getDrawable(requireActivity(), R.drawable.ic_point)
        bind.map.overlays.add(pt)

        val areaAux = BoundingBox.fromGeoPointsSafe(geoPointList)
        bind.map.zoomToBoundingBox(
            areaAux, false, 0,
            16.0, 0
        )

        bind.map.controller.animateTo(geoPoint)

        mapController.setCenter(areaAux.centerWithDateLine)
        mapController.setZoom(16.0)
    }

    override fun onResume() {
        super.onResume()
    }


}