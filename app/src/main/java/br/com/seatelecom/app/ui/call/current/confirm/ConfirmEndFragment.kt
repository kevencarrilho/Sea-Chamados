package br.com.seatelecom.app.ui.call.current.confirm

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import br.com.seatelecom.app.R
import br.com.seatelecom.app.databinding.ConfirmEndUI
import br.com.seatelecom.app.ui.call.current.CallViewModel
import com.google.android.material.tabs.TabLayoutMediator
import com.google.android.material.transition.MaterialSharedAxis

/**
 * A placeholder fragment containing a simple view.
 */
class ConfirmEndFragment : Fragment() {

    private lateinit var bind: ConfirmEndUI
    private lateinit var viewModel: CallViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity())[CallViewModel::class.java]
        val forward = MaterialSharedAxis(MaterialSharedAxis.X, true)
        enterTransition = forward

        val backward = MaterialSharedAxis(MaterialSharedAxis.X, false)
        returnTransition = backward

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bind = ConfirmEndUI.inflate(inflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.title = getString(R.string.concluded_call)
        bind.container.adapter = MenuAdapter(this)
        TabLayoutMediator(bind.tabLayout, bind.container) { tab, position ->
            tab.text = when(position){
                0 -> "Resumo"
                1 -> "Comentários"
                else -> "Mapa"
            }
        }.attach()
        bind.container.isUserInputEnabled = false

        bind.button2.setOnClickListener {

            viewModel.endCall()

//            startActivity(Intent(requireContext(), MainActivity::class.java))
//            requireActivity().finish()
//            val action = ConfirmEndFragmentDirections.actionConfirmEndFragmentToMenuFragment()
//            findNavController().navigate(action)
//            callViewModel.reset()
        }
        return bind.root
    }

    class MenuAdapter(fragment: Fragment ) : FragmentStateAdapter(fragment) {

        override fun getItemCount(): Int = 3

        override fun createFragment(position: Int): Fragment {
            return when(position){
                0 -> ConfirmEndDetailFragment()
                1 -> ConfirmEndRepairFragment()
                else -> ConfirmEndMapsFragment()
            }
        }

    }

}