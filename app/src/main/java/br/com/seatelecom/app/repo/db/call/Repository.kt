package br.com.seatelecom.app.repo.db.call

import android.content.Context
import android.location.Location
import androidx.lifecycle.MutableLiveData
import br.com.seatelecom.app.model.call.*
import br.com.seatelecom.auth.data.model.LoggedInUser
//import br.com.seatelecom.app.core.model.login.User
import io.realm.Realm
import org.bson.types.ObjectId
import java.util.*
import kotlin.collections.ArrayList

class Repository {

    companion object {
        fun update(call: Call) {
            val realm = Realm.getDefaultInstance()
            realm.executeTransaction {
                it.insertOrUpdate(call)
            }
        }

        fun listenerCall(id: ObjectId): MutableLiveData<Call> {
            val response = MutableLiveData<Call>().apply { postValue(Call()) }
            Realm.getDefaultInstance().addChangeListener {
                response.value = get(id)
            }

            return response
        }

        fun get(id: ObjectId): Call {

            var call = Realm.getDefaultInstance().where(Call::class.java)
                .equalTo("id", id).findFirst()

            return call!!

        }


        fun updateStatus(call: Call, status: String){
            val realm = Realm.getDefaultInstance()
            realm.executeTransaction {
                call.status = status
                it.insertOrUpdate(call)
            }
        }

        fun getCurrent(_call: MutableLiveData<Call>): MutableLiveData<Call> {
            val realm = Realm.getDefaultInstance()
            var auxCall = realm.where(Call::class.java)
                .equalTo("current", true).findFirst()
            if (auxCall != null) {
                _call.value = auxCall
                auxCall.addChangeListener { call: Call, changeSet ->
//                    _call.value = get(auxCall.id)
                    _call.value = call
                }

            } else {
                val call = Call()
                realm.executeTransaction {
                    it.insertOrUpdate(call)
                }
                getCurrent(_call)
            }
            return _call
        }

        fun getAll(): ArrayList<Call> {

            ArrayList<Call>().apply {
                this.addAll(
                    Realm.getDefaultInstance().where(Call::class.java).findAll()
                )
                return this
            }
        }

        fun setCreate(
            call: Call?,
            user: LoggedInUser,
            attendance: Attendance?,
            context: Context
        ): Call? {

            user.readUser(context)
            Realm.getDefaultInstance().executeTransaction {
                val st = it.createObject(Attendance::class.java)
                st.set(attendance!!)
                call?.attendance = st
                call?.status = "start"
                call?.operatorId = user.preferredUsername
                val company = user.companies[user.selectedCompany]!!
                call?.companyId = company
                it.insertOrUpdate(call)
            }
            return call
        }

        fun startVerify(call: Call?, status: String): Call? {
            Realm.getDefaultInstance().executeTransaction {
                val br = it.createObject(BeforeRepair::class.java)
                br.startVerify = Calendar.getInstance().timeInMillis
                call?.beforeRepair = br
                call?.status = status
            }
            return call
        }

        fun setDefectLocation(call: Call?, locationAux: Location?): Call? {
            Realm.getDefaultInstance().executeTransaction {
                val location = it.createObject(LocationSeaPoint::class.java)
                location.setLocation(locationAux)
                call?.beforeRepair?.defectLocation = location
                call?.repair?.startRepair = Calendar.getInstance().timeInMillis
            }
            return call
        }

        fun setComment(call: Call?, comment: Comment): Call? {
            Realm.getDefaultInstance().executeTransaction {
                call!!.repair!!.comments.add(comment)
                call.status = "updateFiles"
            }
            return call
        }

        fun enRepair(call: Call?): Call? {
            Realm.getDefaultInstance().executeTransaction {
                call!!.repair!!.endRepair = Calendar.getInstance().timeInMillis
            }
            return call
        }

        fun setMaterialList(call: Call?, material: MaterialSelected): Call? {
            Realm.getDefaultInstance().executeTransaction {
                call?.materialList?.add(material)
            }
            return call
        }

        fun endCall(call: Call?) {
            Realm.getDefaultInstance().executeTransaction {
                call?.end = Calendar.getInstance().timeInMillis
                //call?.current = false
                val s = ""
            }
        }

        fun addLocation(call: Call, location: Location?) {
            Realm.getDefaultInstance().executeTransaction {
                val locationAux = it.createObject(LocationSeaPoint::class.java)
                locationAux.create = Calendar.getInstance().timeInMillis
                locationAux.setLocation(location)
                call.location.add(locationAux)
            }
        }

        fun concludedSelectMaterial(call: Call?): Call? {
            Realm.getDefaultInstance().executeTransaction {
                call?.status = "selectedMaterial"
                it.insertOrUpdate(call)
            }
            return call
        }

        fun deleteItemMaterialList(call: Call?, position: Int) {
            Realm.getDefaultInstance().executeTransaction {
                call?.materialList?.removeAt(position)
            }
        }

        fun SetConclusionDate(call: Call): Call {
            Realm.getDefaultInstance().executeTransaction {
                call.attendance!!.conclusion = Calendar.getInstance().timeInMillis
            }
            return call
        }

    }
}