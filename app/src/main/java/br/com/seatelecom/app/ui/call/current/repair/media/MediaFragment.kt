package br.com.seatelecom.app.ui.call.current.repair.media

import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.seatelecom.app.databinding.MediaUI
import java.io.File

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

/**
 * A simple [Fragment] subclass.
 * Use the [MediaFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MediaFragment : Fragment() {
    private val FILE_NAME = "file"
    private val POSITION = "POSITION"

    // TODO: Rename and change types of parameters
    private var fileName: String? = null
    private var position: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            fileName = it.getString(FILE_NAME)
            position = it.getInt(POSITION)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val bind = MediaUI.inflate(layoutInflater)

        val file = getOutputDirectory(fileName.toString())

        if(file.isFile){
            if(file.extension.equals("jpg")){
                bind.imageView.visibility = View.VISIBLE
                bind.videoView.visibility = View.GONE
                bind.imageView.setImageURI(Uri.parse(file.path))
            } else {
                bind.videoView.setVideoPath(file.path)
                bind.videoView.layoutParams.height = 2000
                bind.videoView.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT

//                val media = MediaController(requireContext())
//                bind.videoView.setMediaController(media)
//                media.setAnchorView(bind.videoView)
                bind.frame.foreground = requireContext().getDrawable(android.R.drawable.ic_media_play)
                bind.frame.foregroundGravity = Gravity.CENTER
                bind.videoView.setOnCompletionListener {
                    bind.frame.foreground = requireContext().getDrawable(android.R.drawable.ic_media_play)
                    bind.frame.foregroundGravity = Gravity.CENTER
                }
                bind.frame.setOnClickListener {
                    if(bind.videoView.isPlaying){
                        bind.frame.foreground = requireContext().getDrawable(android.R.drawable.ic_media_pause)
                        bind.frame.foregroundGravity = Gravity.CENTER
                        bind.videoView.pause()
                    } else {
                        bind.frame.foreground = null
                        bind.videoView.start()
                    }
                }
            }
        }

        return bind.root
    }

    fun getOutputDirectory(fileName: String): File {
        val mediaDir = requireActivity().externalMediaDirs.firstOrNull()?.let {
            File(it, "call/" + fileName) }
        return if (mediaDir != null && mediaDir.exists())
            mediaDir else requireActivity().filesDir
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param fileName Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment MediaFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(fileName: String, position: Int) =
            MediaFragment().apply {
                arguments = Bundle().apply {
                    putString(FILE_NAME, fileName)
                    putInt(POSITION, position)
                }
            }
    }
}