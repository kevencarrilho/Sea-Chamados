package br.com.seatelecom.app.repo.api.call.start

import android.content.Context
import androidx.lifecycle.MutableLiveData
import br.com.seatelecom.app.model.IP_SERVER
import br.com.seatelecom.app.core.model.StartCallService
import br.com.seatelecom.app.model.call.Attendance
import br.com.seatelecom.auth.data.model.LoggedInUser
import br.com.seatelecom.auth.data.model.Token
import com.google.gson.GsonBuilder
import io.realm.Realm
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Repository(){
   companion object{
       private val retrofit = Retrofit.Builder()
           .baseUrl(IP_SERVER)
           .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
           .build()
       private val webService: StartCallService = retrofit.create(
           StartCallService::class.java
       )

       fun get(
           protocol: String,
           _attendance: MutableLiveData<Attendance?>,
           context: Context
       ){
           val token = Token().read(context)
           val user = LoggedInUser().readUser(context)
            token?.let {

                webService.get(it.accessToken,user.companies[user.selectedCompany],protocol).enqueue(
                    object: Callback<Attendance> {
                        override fun onFailure(call2: retrofit2.Call<Attendance>, t: Throwable) {
                            Realm.getDefaultInstance().executeTransaction {
                                _attendance.value = null
                            }
 //                            repo.create(_call)
 //                            call.value = _call
                        }

                        override fun onResponse(
                            call2: retrofit2.Call<Attendance>,
                            response: Response<Attendance>
                        ) {
                            Realm.getDefaultInstance().executeTransaction {

                                if (response.isSuccessful) {
                                    _attendance.value = response.body()
                                } else {
                                    _attendance.value = null
                                }
                            }
 //                            call.value = _call
 //                            repo.create(_call)
                        }

                    })
            }?: run{
                _attendance.value = null
            }
       }
   }
}