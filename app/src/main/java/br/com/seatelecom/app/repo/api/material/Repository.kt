package br.com.seatelecom.app.repo.api.material

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import br.com.seatelecom.app.model.IP_SERVER
import br.com.seatelecom.app.core.model.MaterialService
import br.com.seatelecom.app.model.call.Material
import br.com.seatelecom.auth.data.model.LoggedInUser
import br.com.seatelecom.auth.data.model.Token
import com.google.gson.GsonBuilder
import io.realm.Realm
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Repository(){


    private val retrofit = Retrofit.Builder()
        .baseUrl(IP_SERVER)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .build()
    private val webService: MaterialService = retrofit.create(
        MaterialService::class.java
    )

    fun get(context: Context, materialsCheck: MutableLiveData<Boolean>? = null){

        val token = Token().read(context)
        val user = LoggedInUser().readUser(context)
        if (token!=null && user!=null){
            webService.get(user.companies[user.selectedCompany],token.accessToken)
                .enqueue(object: Callback<ArrayList<Material>> {
                    override fun onResponse(
                        call: retrofit2.Call<ArrayList<Material>>,
                        response: Response<ArrayList<Material>>
                    ) {

                        if (response.isSuccessful){
                            val realm = Realm.getDefaultInstance()
                            realm.executeTransaction{
                                realm.insertOrUpdate(response.body())
                                materialsCheck?.value = true
                            }
                            realm.close()
                        }


                    }

                    override fun onFailure(
                        call: retrofit2.Call<ArrayList<Material>>,
                        t: Throwable
                    ) {
                        Log.e("error", t.message.toString())
                        materialsCheck?.value = true
                    }

                })
        }

    }

}