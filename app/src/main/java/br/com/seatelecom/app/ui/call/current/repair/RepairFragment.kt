package br.com.seatelecom.app.ui.call.current.repair

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.seatelecom.app.R
import br.com.seatelecom.app.databinding.RepairUI
import br.com.seatelecom.app.ui.call.current.CallViewModel
import com.google.android.material.transition.MaterialSharedAxis


class RepairFragment : Fragment() {

    private lateinit var viewModel: CallViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val forward = MaterialSharedAxis(MaterialSharedAxis.X, true)
        enterTransition = forward

        val backward = MaterialSharedAxis(MaterialSharedAxis.X, false)
        returnTransition = backward
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(requireActivity())[CallViewModel::class.java]

        val bind = RepairUI.inflate(inflater)
        bind.commentButton.setOnClickListener {
            findNavController().navigate(R.id.action_tabFragment_to_addCommentFragment)
        }

        viewModel.call.observe(viewLifecycleOwner, Observer {
            val adapter = AdapterComments(it.repair!!.comments, this)
            bind.list.adapter = adapter
            bind.list.layoutManager = LinearLayoutManager(requireContext(),RecyclerView.VERTICAL,false)
//            viewModel.save()
        })

        return bind.root
    }
}