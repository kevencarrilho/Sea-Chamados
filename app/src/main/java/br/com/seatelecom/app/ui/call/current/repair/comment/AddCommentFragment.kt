package br.com.seatelecom.app.ui.call.current.repair.comment

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import br.com.seatelecom.app.R
import br.com.seatelecom.app.databinding.AddCommentUi
import br.com.seatelecom.app.ui.call.current.CallViewModel
import br.com.seatelecom.app.ui.call.current.repair.media.AdapterMedia
import com.iceteck.silicompressorr.SiliCompressor
import java.io.File
import java.io.IOException

class AddCommentFragment : DialogFragment() {

    lateinit var bind: AddCommentUi
    lateinit var viewModelComment: AddCommentViewModel
    lateinit var viewModel: CallViewModel
    private var file: File? = File("")
    lateinit var uriFile: Uri
    lateinit var adapter: AdapterMedia
    private var takeVideo: ActivityResultLauncher<Uri>? = null
    private var takePicture: ActivityResultLauncher<Uri>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelComment = ViewModelProvider(this)[AddCommentViewModel::class.java]
        viewModel = ViewModelProvider(requireActivity())[CallViewModel::class.java]

        takeVideo =
            registerForActivityResult(ActivityResultContracts.TakeVideo()) {
                file?.let { file ->
                    val space = file.length()
                    if (space > 0L) {
                        compressVideo(file)
                    } else {
                        file.delete()
                    }
                }
            }
        takePicture =
            registerForActivityResult(ActivityResultContracts.TakePicture()) {
                file?.let { file ->
                    val path = file.path
                    if (it) {
                        viewModelComment.comment.files.add(file.name)
                        compressImage(file)
                    }
                }
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        bind = AddCommentUi.inflate(layoutInflater)
        (requireActivity() as AppCompatActivity).supportActionBar?.title =
            getString(R.string.comment)
        adapter = AdapterMedia(this, viewModelComment.comment.files)
        bind.viewPager2.adapter = adapter

        bind.picture.setOnClickListener {
            viewModelComment.comment.text = bind.text.text.toString()
            dispatchTakePictureIntent()
        }

        bind.video.setOnClickListener {
            viewModelComment.comment.text = bind.text.text.toString()
            dispatchTakeVideoIntent()
        }

        bind.button4.setOnClickListener {
            val comment = viewModelComment.comment
            comment.text = bind.text.text.toString()
            for (aux in comment.files) comment.filesSync.add(false)
            viewModel.setComment(comment)

            findNavController().popBackStack()
        }



        bind.delete.setOnClickListener {
            viewModelComment.comment.files.removeAt(bind.viewPager2.currentItem)
            adapter.files = viewModelComment.comment.files
            adapter.notifyItemRemoved(bind.viewPager2.currentItem)
            if (viewModelComment.comment.files.size > 0) {
                bind.button4.isEnabled = true
                bind.delete.isEnabled = true
                adapter.files = viewModelComment.comment.files
                adapter.notifyDataSetChanged()
                bind.cardView.layoutParams.height = 0
            } else {
                bind.button4.isEnabled = false
                bind.delete.isEnabled = false

                bind.cardView.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            }
        }
        return bind.root
    }


    override fun onResume() {
        super.onResume()

        bind.text.setText(viewModelComment.comment.text)

        if (viewModelComment.comment.files.size != 0) {
            bind.button4.isEnabled = true
            bind.delete.isEnabled = true
            adapter.files = viewModelComment.comment.files
            adapter.notifyDataSetChanged()
            bind.cardView.layoutParams.height = 0
        } else {
            bind.button4.isEnabled = false
            bind.delete.isEnabled = false

            bind.cardView.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
        }
    }


    private fun dispatchTakePictureIntent() {
        file = try {
            createImageFile()
        } catch (ex: IOException) {

            null
        }
        file?.also { file ->
            uriFile = FileProvider.getUriForFile(
                requireContext(),
                "br.com.seatelecom.seachamados",
                file
            )

            verifyPermission()
        }
    }

    fun verifyPermission() {
        try { //Checa as permissões
            takePicture!!.launch(uriFile)
        } catch (ex: SecurityException) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(
                    Manifest.permission.CAMERA,
                ), 0
            )  //pede a permissão ao usuário caso esteja negado
        }finally {
        }
    }


    private fun compressImage(file: File) {
        val image = BitmapFactory.decodeFile(file.path)

        image.compress(Bitmap.CompressFormat.JPEG, 60, file.outputStream())
    }

    private fun dispatchTakeVideoIntent() {
        file = try {
            createVideoFile()
        } catch (ex: IOException) {

            null
        }
        file?.also { file ->
            uriFile = FileProvider.getUriForFile(
                requireContext(),
                "br.com.seatelecom.seachamados",
                file
            )
            val boll = (ActivityCompat.checkSelfPermission(//Verifica se tem permissão
                requireActivity(),
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_DENIED
                    )
            if (boll) {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(Manifest.permission.CAMERA), 0
                )  //pede a permissão ao usuário caso esteja negado
            } else {
                takeVideo!!.launch(uriFile)
            }
        }

    }

    private fun compressVideo(file: File) {

        viewModelComment.comment.files.add(file.name)
        Thread {
            val filePath = SiliCompressor.with(requireContext())
                .compressVideo(file.path, getOutputDirectory().path, 1280, 720, 1000000)
            val arq = File(filePath)
            val name = file.name
            arq.renameTo(file)
        }.start()
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val storageDir: File? = getOutputDirectory()
        return File.createTempFile(
            viewModel.call.value?.id.toString() + "_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        )
    }

    @Throws(IOException::class)
    private fun createVideoFile(): File {
        // Create an image file name
        val storageDir: File? = getOutputDirectory()
        return File.createTempFile(
            viewModel.call.value?.id.toString() + "_", /* prefix */
            ".mp4", /* suffix */
            storageDir /* directory */
        )
    }

    private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
    private val REQUEST_CODE_PERMISSIONS = 10
    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            requireContext(), it
        ) == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                dispatchTakePictureIntent()
            } else {
                Toast.makeText(
                    requireContext(),
                    "Permissions not granted by the user.",
                    Toast.LENGTH_SHORT
                ).show()

            }
        }
    }

    fun getOutputDirectory(): File {
        val mediaDir = requireActivity().externalMediaDirs.firstOrNull()?.let {
            File(it, "call").apply { mkdirs() }
        }
        return if (mediaDir != null && mediaDir.exists())
            mediaDir else requireActivity().filesDir
    }

}