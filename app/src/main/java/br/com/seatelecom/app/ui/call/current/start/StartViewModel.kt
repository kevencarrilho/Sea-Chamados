package br.com.seatelecom.app.ui.call.current.start

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class StartViewModel: ViewModel() {

    private var array = arrayOf(false, false)
    private val _steps = MutableLiveData<Array<Boolean>>().apply {
        value = array
    }
    val steps: LiveData<Array<Boolean>> = _steps

    fun setInitVerify(){
        array = _steps.value!!
        array[0] = true
        _steps.value = array
    }

    fun setDefectLocation(){
        array = _steps.value!!
        array[1] = true
        _steps.value = array
    }
}