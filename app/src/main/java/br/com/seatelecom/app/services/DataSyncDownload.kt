package br.com.seatelecom.app.services

import android.app.Service
import android.content.Intent
import android.os.IBinder
import br.com.seatelecom.app.repo.api.material.Repository as MaterialApi

class DataSyncDownload() : Service() {

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {


        getAllMaterialsApi()

        //return super.onStartCommand(intent, flags, startId)
        return START_STICKY
    }

    fun getAllMaterialsApi(){
        MaterialApi().get(applicationContext)
    }

}