package br.com.seatelecom.app.ui.call.current

import android.app.Application
import android.location.Location
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.seatelecom.app.model.call.*
import br.com.seatelecom.app.repo.api.call.start.Repository
import br.com.seatelecom.auth.data.model.Token
import io.realm.RealmList
import br.com.seatelecom.auth.data.model.LoggedInUser as User
import kotlin.collections.ArrayList
import br.com.seatelecom.app.repo.db.material.Repository as MaterialDB
import br.com.seatelecom.app.repo.api.material.Repository as MaterialApi
import br.com.seatelecom.app.repo.db.call.Repository as CallDB
import br.com.seatelecom.app.repo.api.call.Repository as CallApi

class CallViewModel(application: Application) : AndroidViewModel(application) {

    private var _call = br.com.seatelecom.app.repo.db.call.Repository.getCurrent(MutableLiveData())
//    private val user = User.Repository().getLiveData()
//    private val token = Token.Repository().getTokenLiveData()

    private val _materialsCheck = MutableLiveData<Boolean>().apply {
        value = true
    }

    private val _materials = MutableLiveData<ArrayList<Material>>().apply {
        MaterialDB.get()
    }

    private val _start = MutableLiveData<Attendance?>()

    val materialsCheck: LiveData<Boolean> = _materialsCheck
    val materials: LiveData<ArrayList<Material>> = _materials
    val attendance: LiveData<Attendance?> = _start
    val call: LiveData<Call> = _call

    fun getNewCurrent() {
        _call = br.com.seatelecom.app.repo.db.call.Repository.getCurrent(MutableLiveData())
    }

    fun create() {
        _call.value = CallDB.setCreate(_call.value, User(), _start.value, getApplication())
        val s = ""
        //CallApi.create(_call.value!!, Token().read(getApplication()))
    }

    fun getStart(protocol: String) {
        Repository.get(protocol, _start, getApplication())
    }

    fun getMaterial() {
        MaterialDB.get(_materialsCheck)
    }

    fun getMaterialApi() {
        MaterialApi().get(getApplication(), _materialsCheck)
    }

//    fun update(call: Call){
//        _call.value = call
//        repo.update(call)
//    }

//    fun save(){
//        repo.update(_call.value!!)
//    }

    fun startVerify() {
        _call.value =
            br.com.seatelecom.app.repo.db.call.Repository.startVerify(_call.value, "startVerify")
    }

    fun setDefectLocation(locationAux: Location?) {
        _call.value = br.com.seatelecom.app.repo.db.call.Repository.setDefectLocation(
            _call.value,
            locationAux
        )
    }

    fun setConclusionDate() {
        _call.value = br.com.seatelecom.app.repo.db.call.Repository.SetConclusionDate(_call.value!!)
    }

    fun setComment(comment: Comment) {
        val call = _call.value
        _call.value = br.com.seatelecom.app.repo.db.call.Repository.setComment(_call.value, comment)
    }

    fun endRepair() {
        _call.value = br.com.seatelecom.app.repo.db.call.Repository.enRepair(_call.value)
    }

    fun setMaterialList(material: MaterialSelected) {
        _call.value =
            br.com.seatelecom.app.repo.db.call.Repository.setMaterialList(_call.value, material)
    }

    fun endCall() {
        br.com.seatelecom.app.repo.db.call.Repository.endCall(_call.value)
//        _call = br.com.seatelecom.app.repo.db.call.Repository.getCurrent(MutableLiveData())
    }

    fun getMaterialsOfCategory(category: String): ArrayList<Material> {
        _materials.value = br.com.seatelecom.app.repo.db.material.Repository.getCategory(
            category,
            _call.value!!.companyId
        )

        return _materials.value!!
    }

    fun concludedSelectMaterial() {
        _call.value =
            br.com.seatelecom.app.repo.db.call.Repository.concludedSelectMaterial(_call.value)
    }

    fun removeMaterial(position: Int) {
        br.com.seatelecom.app.repo.db.call.Repository.deleteItemMaterialList(
            _call.value!!,
            position
        )
    }


}