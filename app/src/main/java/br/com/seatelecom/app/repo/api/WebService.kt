package br.com.seatelecom.app.core.model

import br.com.seatelecom.app.model.call.Material
import br.com.seatelecom.app.model.call.Attendance
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*
import kotlin.collections.ArrayList

interface StartCallService{
    @GET("protocol")
    fun get(
        @Query("access_token") token: String?,
        @Query("company") company: String?,
        @Query("protocol") protocol: String
    ):Call<Attendance>
}

interface CallService{
    @GET("call")
    fun get(
        @Query("access_token") token: String?,
        @Query("company") company: String?
    ) : Call<ArrayList<br.com.seatelecom.app.model.call.Call>>

    @POST("call")
    fun create(
        @Query("access_token") token: String?,
        @Query("company") company: String?,
        @Body objCall: JsonObject
    ) : Call<JsonObject>

    @Multipart
    @POST("call/{id}/{index}")
    fun uploadFile(
        @Path("id") id: String,
        @Path("index") index: Int,
        @Query("access_token") token: String?,
        @Query("company") company: String?,
        @Part file: MultipartBody.Part
    ) : Call<ResponseBody>

    @PUT("call/{id}")
    fun update(
        @Path("id") id: String,
        @Query("access_token") token: String?,
        @Query("company") company: String?,
        @Body objCall: JsonObject
    ) : Call<br.com.seatelecom.app.model.call.Call>
}







interface MaterialService{

    @GET("material")
    fun get(
        @Query("company") company: String?,
        @Query("access_token") token: String?
    ): Call<ArrayList<Material>>
}

interface UserService {

    @GET("user/info")
    fun infoUser(
        @Query("access_token") token: String?
    ): Call<JsonObject>
}
