package br.com.seatelecom.app.ui.call.current.material

import android.Manifest
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Bundle
import android.text.InputType
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.*
import br.com.seatelecom.app.R
import br.com.seatelecom.app.databinding.MaterialItemUi
import br.com.seatelecom.app.databinding.MaterialListUI
import br.com.seatelecom.app.model.call.Material
import br.com.seatelecom.app.model.call.MaterialSelected
import br.com.seatelecom.app.services.DataSyncDownload
import br.com.seatelecom.app.ui.call.current.CallViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.transition.MaterialSharedAxis
import io.realm.RealmList


class MaterialListFragment : Fragment() {
    private lateinit var viewModel: CallViewModel
    private lateinit var bind: MaterialListUI


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity())[CallViewModel::class.java]

        val forward = MaterialSharedAxis(MaterialSharedAxis.X, true)
        enterTransition = forward

        val backward = MaterialSharedAxis(MaterialSharedAxis.X, false)
        returnTransition = backward


    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val serviceIntent = Intent(requireContext(), DataSyncDownload::class.java)
        requireActivity().startService(serviceIntent)

        bind = MaterialListUI.inflate(inflater)

        bind.selectMaterial.setOnClickListener {
            getMaterial(bind.category.selectedItem.toString())
        }

        viewModel.call.observe(viewLifecycleOwner, Observer {
            bind.list.adapter = MaterialAdapter(it.materialList)
            bind.list.layoutManager =
                LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)

        })

        bind.refreshLayout.setOnRefreshListener {
            viewModel.getMaterialApi()
            //requireActivity().startService(serviceIntent)
        }

//        viewModel.materials.observe(viewLifecycleOwner, Observer {
//            bind.refreshLayout.isRefreshing = false
//        })

        viewModel.materialsCheck.observe(viewLifecycleOwner, Observer {
            if (!it) {
                Snackbar.make(
                    bind.root,
                    "Não foi possivel atualizar a lista de materiais",
                    Snackbar.LENGTH_SHORT
                ).show()
            }
            bind.refreshLayout.isRefreshing = false
        })

        return bind.root
    }


    private fun getMaterial(category: String) {
        //avaliação de permissao de midia
        if ((ActivityCompat.checkSelfPermission(//Verifica se tá sem permissão
                requireActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) and
                    (ActivityCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ))) == PackageManager.PERMISSION_DENIED
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 0
            )
        }
        if (category == "Selecionar") {

            Snackbar.make(
                bind.constraint,
                "Selecione uma categoria",
                Snackbar.LENGTH_SHORT
            )
                .show()
        } else {

            val materials = viewModel.getMaterialsOfCategory(category)

            val adapter = ArrayAdapter<Material>(
                requireContext(),
                R.layout.item_simple,
                R.id.textView_item_simple,
                materials
            )
            AlertDialog.Builder(requireContext())
                .setTitle("Lista de Material")
                .setAdapter(adapter, DialogInterface.OnClickListener { dialog, which ->

                    val input = EditText(requireContext());
                    input.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    input.setRawInputType(Configuration.KEYBOARD_12KEY);
                    val param = bind.textView10.layoutParams as ViewGroup.MarginLayoutParams
                    param.setMargins(16, 16, 16, 16)
                    input.layoutParams = param

                    val material = MaterialSelected()
                    material.set(materials.get(which))


//                    AlertDialog.Builder(requireContext())
//                        .setTitle("Quantidade de Material")
//                        .setView(input)
//                        .setNegativeButton("Cancelar") { dialog, which ->
//
//                        }
//                        .setPositiveButton("Concluir") { dialog, which ->
//                            try {
//                                material.amount = input.text.toString().toDouble()
////                            material._id = ""
//                                viewModel.setMaterialList(material)
//                            } catch (e: NumberFormatException) {
//                                Log.e("", "")
//                            }
//                        }
//                        .create().show()

                    val dialog = androidx.appcompat.app.AlertDialog.Builder(requireContext())
                        .setTitle("Quantidade de Material")
                        .setView(input)
                        .setPositiveButton("Concluir", null)
                        .setNegativeButton("Cancelar", null)
                        .create()
                    dialog.setOnShowListener {
                        val btnPositive =
                            dialog.getButton(androidx.appcompat.app.AlertDialog.BUTTON_POSITIVE)
                        btnPositive.setOnClickListener {
                            if (input.text.toString()
                                    .isNotEmpty()
                            ) {
                                if (input.text.toString().toInt() > 0) {
                                    try {
                                        material.amount = input.text.toString().toDouble()
//                            material._id = ""
                                        viewModel.setMaterialList(material)
                                    } catch (e: NumberFormatException) {
                                        Log.e("", "")
                                    }
                                    dialog.dismiss()
                                } else {
                                    Toast.makeText(
                                        context,
                                        "Preencha uma quantidade válida!",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }

                            }else{
                                Toast.makeText(
                                    context,
                                    "Preencha a quantidade!",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }
                    dialog.show()

                }).create().show()

        }

    }

    private inner class ListAdapterMaterial() :
        ListAdapter<MaterialSelected, MaterialViewHolder>(
            object : DiffUtil.ItemCallback<MaterialSelected>() {
                override fun areItemsTheSame(
                    oldItem: MaterialSelected,
                    newItem: MaterialSelected
                ): Boolean {
                    return true
                }

                override fun areContentsTheSame(
                    oldItem: MaterialSelected,
                    newItem: MaterialSelected
                ): Boolean {
                    return true
                }
            }
        ) {

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): MaterialViewHolder {
            return MaterialViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_material, parent, false)
            )
        }

        override fun onBindViewHolder(holder: MaterialViewHolder, position: Int) {

            val material = getItem(position)
            holder.binding.name.text = material!!.name
            if (material.amount == 0.0) {
                holder.binding.amount.text = "Unidade métrica: " + material.amount
            } else {
                holder.binding.amount.text =
                    "Quantidade: " + material.amount + " " + material.amount
            }
        }

    }

    class MaterialViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding: MaterialItemUi

        init {
            binding = DataBindingUtil.bind(view)!!
        }
    }

    inner class MaterialAdapter(var list: RealmList<MaterialSelected>) :
        RecyclerView.Adapter<MaterialViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MaterialViewHolder {
            return MaterialViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_material, parent, false)
            )
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onBindViewHolder(modelMaterial: MaterialViewHolder, position: Int) {
            val material = list.get(position)

            modelMaterial.binding.name.text = material!!.name
            modelMaterial.binding.amount.text =
                "Quantidade: " + material.amount + "\nUnidade: " + material.unity

            modelMaterial.binding.delete.setOnClickListener {
                viewModel.removeMaterial(position)
                modelMaterial.binding.notifyChange()
            }

        }

    }
}