package br.com.seatelecom.app.ui.root

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import br.com.seatelecom.app.R
import br.com.seatelecom.app.databinding.FragmentCallMenuBinding
import br.com.seatelecom.app.model.CALL_ID
import br.com.seatelecom.app.services.Displacement
import br.com.seatelecom.app.ui.call.current.CallViewModel
import br.com.seatelecom.auth.data.model.LoggedInUser
import br.com.seatelecom.auth.data.model.Token
import java.lang.Exception

class MenuFragment: Fragment() {

    private lateinit var binding: FragmentCallMenuBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_call_menu, container, false)


        val actionBar = (requireActivity() as AppCompatActivity).supportActionBar!!
        actionBar.show()
        actionBar.title = getString(R.string.app_name)

        try{
            val token = Token().read(requireContext())
            val user = LoggedInUser().readUser(requireContext())

            if(token.accessToken != null && user.name != "") {
                setHasOptionsMenu(true)
                buttonCorrective()
                buttonPreventive()
                buttonSwap()
            }else{
                logout()
            }
        }catch (e: Exception){
            Log.e("erro", e.message.toString())
            logout()
        }

        return binding.root
    }

    /*private fun login(){
        val viewModel = ViewModelProvider(requireActivity()).get(LoginViewModel::class.java)

        if(viewModel.user.value==null){
            findNavController().navigate(Uri.parse("auth://loginFragment"))
        } else {
            val login = ViewModelProvider(requireActivity()).get(LoginViewModel::class.java)
            login.user.value?.let {

                val settings: SharedPreferences = requireActivity().getSharedPreferences("PASSWORD", 0)
                if (login.token.value?.accessToken==null) {
                    val password = settings.getString("password","").toString()
                    login.login(it.preferredUsername, password)
                }
            }

            login.token.observe(viewLifecycleOwner, Observer {
                it?.let{
                    if (it.accessToken !=""){
//                        val intent = Intent(requireActivity(), ProjectSync::class.java)
//                        requireActivity().startService(intent)
                    }
                }
            })


        }
        val actionBar = (requireActivity() as AppCompatActivity).supportActionBar!!
        actionBar.show()
        actionBar.title = getString(R.string.app_name)
    }*/

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun buttonCorrective(){
        val viewModel = ViewModelProvider(requireActivity())[CallViewModel::class.java]
        //Verificação se existe uma call ativa (nova ou em estado de finalização)


        //ok, não tem call ativa, continuamos com a construção do menu // revisar essa bagaça
        viewModel.call.observe(this@MenuFragment.viewLifecycleOwner, Observer{
            if ((it?.status!="end").and(it?.status!="new")){

//                val serviceIntent = Intent(requireContext(), Displacement::class.java)
//                serviceIntent.putExtra(CALL_ID, it.id.toString())
//                requireActivity().startService(serviceIntent)

            }else if(it?.status=="end"){
                viewModel.getNewCurrent()
                //NavHostFragment.findNavController(this).popBackStack()
            }
            binding.corretive.setCompoundDrawablesWithIntrinsicBounds(
                ContextCompat.getDrawable
                    (requireContext(), R.drawable.ic_circular_refresh_arrows),
                null, null, null)
            binding.corretive.text = "Chamado em\nandamento"
            var action: Int

            if (it?.status=="new"){

                action = R.id.action_menuFragment_to_createCallFragment
                binding.corretive.setCompoundDrawablesWithIntrinsicBounds(
                    requireContext().getDrawable(R.drawable.ic_band_aid),
                    null, null, null)
                binding.corretive.text = "Corretivo"

            } else if ((it?.beforeRepair?.defectLocation==null)
                    .and(it?.attendance?.protocol!="")){

                action = R.id.action_menuFragment_to_startFragment

            } else if ((it?.beforeRepair?.defectLocation!=null)
                    .and(it?.repair?.endRepair==0L)) {

                action = R.id.action_menuFragment_to_tabFragment

            } else if((it?.repair?.endRepair!=0L)
                    .and(!(it?.status=="selectedMaterial"))
            ){

                action = R.id.action_menuFragment_to_tabFragment

            } else if(it?.status=="selectedMaterial"){

                action = R.id.action_menuFragment_to_finalFragment

            }else if(it?.status=="updateFilesConcluded"){
                action = R.id.action_menuFragment_to_createCallFragment
            }
            else {

                action = R.id.action_menuFragment_to_createCallFragment

                binding.corretive.setCompoundDrawablesWithIntrinsicBounds(
                    requireContext().getDrawable(R.drawable.ic_emergency_call),
                    null, null, null)
                binding.corretive.text = "Novo\nChamado"
            }
            binding.corretive.setOnClickListener {
//                        if(viewModel.call.value?.start?.protocol==""){
//                            val action = MenuFragmentDirections.actionMenuFragmentToCreateCallFragment()
//                            findNavController().navigate(action)
//                        } else {
//                            Snackbar.make(holder.binding.imageView,"Finalize o chamado em aberto para iniciar um novo", Snackbar.LENGTH_LONG).show()
//                        }
                findNavController().navigate(action)

            }
        })
    }

    private fun buttonPreventive(){
        //revisar
        binding.preventive.setCompoundDrawablesWithIntrinsicBounds(
            ContextCompat.getDrawable
                (requireContext(), R.drawable.ic_police_line),
            null, null, null)
        binding.corretive.text = "Preventivo"
    }

    private fun buttonSwap(){
        //revisar2, falta o action
        binding.swap.setCompoundDrawablesWithIntrinsicBounds(
            ContextCompat.getDrawable
                (requireContext(), R.drawable.ic_resource_switch),
            null, null, null)
        binding.corretive.text = "Swap"

    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.loginFragment -> {
                val user = LoggedInUser()
                user.writeUser(requireContext())
                val token = Token()
                token.write(requireContext())
                //NavHostFragment.findNavController(this).popBackStack()
                //NavHostFragment.findNavController(this).navigateUp()
                logout()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun logout(){
        val uri = Uri.parse("auth://loginFragment")
        NavHostFragment.findNavController(this).navigate(uri)
    }
}