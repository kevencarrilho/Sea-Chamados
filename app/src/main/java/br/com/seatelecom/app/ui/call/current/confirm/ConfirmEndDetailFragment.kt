package br.com.seatelecom.app.ui.call.current.confirm

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.seatelecom.app.databinding.ConfirmEndDetailUI
import br.com.seatelecom.app.ui.call.current.CallViewModel
import br.com.seatelecom.auth.login.LoginViewModel
import com.google.android.material.transition.MaterialSharedAxis
import java.text.SimpleDateFormat
import java.util.*

class ConfirmEndDetailFragment : Fragment() {


    private lateinit var bind: ConfirmEndDetailUI
    private lateinit var viewModel: CallViewModel
    private lateinit var login: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        login = ViewModelProvider(requireActivity())[LoginViewModel::class.java]
        viewModel = ViewModelProvider(requireActivity())[CallViewModel::class.java]
        val forward = MaterialSharedAxis(MaterialSharedAxis.X, true)
        enterTransition = forward

        val backward = MaterialSharedAxis(MaterialSharedAxis.X, false)
        returnTransition = backward

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        bind = ConfirmEndDetailUI.inflate(inflater)

        viewModel.call.value?.let{

            bind.clientName.text = it.attendance!!.nameClient
            bind.description.text = it.attendance!!.description
            bind.operator.text = it.attendance!!.operator
            bind.protocol.text = it.attendance!!.protocol

            var calendar = Calendar.getInstance()

            calendar.timeInMillis = it.beforeRepair!!.startVerify
            bind.initVerify.text = SimpleDateFormat("dd/MM/yyyy HH:mm").format(calendar.time)
            calendar.timeInMillis = it.beforeRepair!!.defectLocation!!.create
            bind.defectLocation.text = SimpleDateFormat("dd/MM/yyyy HH:mm").format(calendar.time)

            calendar.timeInMillis = it.repair!!.startRepair
            bind.startRepair.text = SimpleDateFormat("dd/MM/yyyy HH:mm").format(calendar.time)
            calendar.timeInMillis = it.repair!!.endRepair
            bind.endRepair.text = SimpleDateFormat("dd/MM/yyyy HH:mm").format(calendar.time)

           // bind.materials.adapter = MaterialAdapter(it.materialList)

            bind.materials.layoutManager = LinearLayoutManager(requireContext(),
                RecyclerView.VERTICAL,false)
        }
        return bind.root
    }

}