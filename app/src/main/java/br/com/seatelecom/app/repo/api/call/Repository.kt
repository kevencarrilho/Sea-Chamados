package br.com.seatelecom.app.repo.api.call

import android.content.Context
import android.util.Log
import br.com.seatelecom.auth.data.model.LoggedInUser
import br.com.seatelecom.auth.data.model.Token
import br.com.seatelecom.app.core.model.CallService
import br.com.seatelecom.app.model.IP_SERVER
import br.com.seatelecom.app.repo.db.call.Repository
import br.com.seatelecom.app.model.call.Call
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import io.realm.Realm
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import kotlin.collections.ArrayList

class Repository {
    companion object {
        private val retrofit = Retrofit.Builder()
            .baseUrl(IP_SERVER)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .build()

        private val webService = retrofit.create(
            CallService::class.java
        )

        fun create(call: Call, token: Token? = null) {
            val json = call.getJson()
            if (token != null) {
                webService.create(token.accessToken, call.companyId, call.getJson())
                    .enqueue(object : Callback<JsonObject> {
                        override fun onResponse(
                            callback: retrofit2.Call<JsonObject>,
                            response: Response<JsonObject>
                        ) {

                            if (response.isSuccessful) {
                                val realm = Realm.getDefaultInstance()
                                realm.executeTransaction {
                                    call.createSync = false
                                    it.insertOrUpdate(call)
                                }
                            }
                        }

                        override fun onFailure(call: retrofit2.Call<JsonObject>, t: Throwable) {

                            val erro = t.message
                            print(erro)
                        }
                    })
            }
        }

        fun update(
            call: Call,
            filesStatus: ArrayList<Boolean> = ArrayList(),
            token: Token? = null
        ) {
            val json = call.getJson()
            if (token != null) {
                val test = webService.update(
                    call.id.toString(),
                    token.accessToken,
                    call.companyId,
                    json
                )
                test.enqueue(object : Callback<Call> {
                    override fun onResponse(
                        callback: retrofit2.Call<Call>,
                        response: Response<Call>
                    ) {
                        val s = call
                        val t = filesStatus
                        if (response.isSuccessful) {
                            if ((call.end != 0L).and(!filesStatus.contains(false))
                                    .and(call.status != "end")
                            ) {
                                Realm.getDefaultInstance().executeTransaction {
                                    call.status = "end"
                                    call.current = false
                                }

                                Repository.update(call)

                            }
                        }
                    }

                    override fun onFailure(call: retrofit2.Call<Call>, t: Throwable) {
                        Log.e("Network error", t.localizedMessage.toString())
                    }
                })


            }
        }

        fun uploadFile(call: Call, i: Int, j: Int, mediaDir: File, context: Context) {
            val token = Token().read(context)
            val id = call.id
            val file = File(mediaDir, "call/" + call.repair!!.comments[i]!!.files[j]!!)
            val body = MultipartBody.Part.createFormData(
                "file",
                file.name,
                RequestBody.create(MediaType.parse("image/jpg"), file)
            )
             val call2 = Repository.get(id)
            val s = call.repair?.comments!![i]!!.filesSync[j]
            webService.uploadFile(call.id.toString(), i, token?.accessToken, call.companyId, body)
                .enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(
                        callback: retrofit2.Call<ResponseBody>,
                        response: Response<ResponseBody>
                    ) {
                        if (response.isSuccessful) {
                            val callRepo = Repository.get(id)
                            Realm.getDefaultInstance().executeTransaction {
                                callRepo.repair?.comments!![i]!!.filesSync[j] = true
                                it.copyToRealmOrUpdate(callRepo)
                            }
                        } else {
                            val error = response.errorBody()
                            Log.e("ErroUploadFile", error.toString())
                        }
                    }

                    override fun onFailure(
                        call: retrofit2.Call<ResponseBody>,
                        t: Throwable
                    ) {
                        Log.e("", t.message.toString())
                    }

                })

        }

        fun get(token: Token?, user: LoggedInUser) {
            webService.get(
                token?.accessToken,
                user.companies[user.selectedCompany]
            ).enqueue(object : Callback<ArrayList<Call>> {
                override fun onResponse(
                    call: retrofit2.Call<ArrayList<Call>>,
                    response: Response<ArrayList<Call>>
                ) {
                    if (response.isSuccessful) {
                        Realm.getDefaultInstance().executeTransaction {
                            it.insertOrUpdate(response.body())
                        }
                    }
                }

                override fun onFailure(call: retrofit2.Call<ArrayList<Call>>, t: Throwable) {
                    print(t)
                }
            })
        }
    }

}