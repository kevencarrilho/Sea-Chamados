package br.com.seatelecom.app.services

import android.Manifest
import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.core.app.ActivityCompat
import br.com.seatelecom.app.R
import br.com.seatelecom.auth.data.model.Token
import br.com.seatelecom.app.model.CALL_ID
import br.com.seatelecom.app.model.CHANNEL_ID
import br.com.seatelecom.app.model.ONGOING_NOTIFICATION_ID
import br.com.seatelecom.app.repo.db.call.Repository
import br.com.seatelecom.app.repo.api.call.Repository as CallAPI
import org.bson.types.ObjectId
import java.lang.Exception
import kotlin.collections.ArrayList

class Displacement() : Service() {


    private lateinit var locationManager: LocationManager
    private lateinit var myLocation: MyLocation
    private val oldId = ArrayList<String>()
    private val repo = CallAPI()

    //    private val token = Token.Repository()
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val callId = intent?.extras?.get(CALL_ID)

        callId.let {
            if (!oldId.contains(it.toString())) {

                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
                    myLocation = MyLocation(it.toString(), intent)
                    locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        600L,
                        10f,
                        myLocation
                    )
                    startNotification(intent)
                }
                startNotification(intent)
            }
        }

        return START_STICKY
    }

    private fun startNotification(intent: Intent?) {
        val pendingIntent: PendingIntent =
            PendingIntent.getActivity(this, 0, intent, 0)

        val notification: Notification =
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                Notification.Builder(this, CHANNEL_ID)
                    .setContentTitle("Acompanhamento de Chamado")
                    .setContentText("Essa notificação é necessária para coletar corretamente a sua localização")
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .build()
            } else {
                Notification.Builder(this)
                    .setContentTitle("Acompanhamento de Chamado")
                    .setContentText("Essa notificação é necessária para coletar corretamente a sua localização")
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .build()
            }

        startForeground(ONGOING_NOTIFICATION_ID, notification)

    }

    private fun stopNotification(intent: Intent?) {
        locationManager.removeUpdates(myLocation)
        stopForeground(true)
        stopSelf()

    }

    private inner class MyLocation(val id: String, val intent: Intent?) : LocationListener {

        override fun onLocationChanged(it: Location) {

            try {
                val token = Token().read(applicationContext)
                val call = Repository.get(ObjectId(id))
                val status = call.status

                if (status == "end") stopNotification(intent)

                else Repository.addLocation(call, it)

                if (call.createSync) {
//                    call.location.add(location)
                    br.com.seatelecom.app.repo.api.call.Repository.create(call, token)
                } else {


                    Repository.update(call)
                    call.repair?.comments?.let { comments ->

                        val bools = ArrayList<Boolean>()
                        for ((i, comment) in comments.withIndex()) {
                            for ((j, sync) in comment.filesSync.withIndex()) {
                                if (!sync) {
                                    br.com.seatelecom.app.repo.api.call.Repository.uploadFile(
                                        call,
                                        i,
                                        j,
                                        externalMediaDirs[0],
                                        applicationContext
                                    )
                                }
                                bools.add(sync)
                            }
                        }
                        if ((!bools.contains(false)).and(call.status == "updateFiles")) {
                            Repository.updateStatus(call, "updateFilesConcluded")
                        }


                        br.com.seatelecom.app.repo.api.call.Repository.update(
                            call,
                            bools,
                            token
                        )
                    }
                }
            } catch (e: Exception) {
                Log.e("err", e.toString())
            }
        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

        }

    }
}