package br.com.seatelecom.app.ui.call.current.conclude

import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import br.com.seatelecom.app.R
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import br.com.seatelecom.app.App
import br.com.seatelecom.app.databinding.FinalCallUI
import br.com.seatelecom.app.ui.call.current.CallViewModel
import java.util.*

class FinalFragment : Fragment() {

    private lateinit var viewModel: CallViewModel
    private lateinit var bind: FinalCallUI

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity())[CallViewModel::class.java]
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        bind = FinalCallUI.inflate(inflater)

        val actionBar = (requireActivity() as AppCompatActivity).supportActionBar!!
        actionBar.show()
        actionBar.title = getString(R.string.final_attendance)

        viewModel.call.observe(viewLifecycleOwner,{

            val attendence = it.attendance

            bind.clientName.text = it.attendance!!.nameClient
            bind.description.text = it.attendance!!.description
            bind.operator.text = it.attendance!!.operator
            bind.protocol.text = it.attendance!!.protocol

            val cal = Calendar.getInstance()
            cal.timeInMillis = it.attendance!!.create
            bind.hour.text = SimpleDateFormat("dd/MM/yyyy hh:mm")
                .format(cal.time)

            bind.startVerify.text = SimpleDateFormat("dd/MM/yyyy hh:mm")
                .format(it.beforeRepair!!.startVerify)

            bind.startLocation.text = SimpleDateFormat("dd/MM/yyyy hh:mm")
                .format(it.beforeRepair!!.defectLocation!!.create)

            bind.finalTextData.text = SimpleDateFormat("dd/MM/yyyy hh:mm")
                .format(it.attendance!!.conclusion)
        })


        bind.comentarios.setOnClickListener {
            findNavController().navigate(R.id.action_finalFragment_to_confirmEndRepairFragment)
        }
        bind.materiais.setOnClickListener {
            findNavController().navigate(R.id.action_finalFragment_to_confirmEndMaterialsFragment)
        }
        bind.mapa.setOnClickListener {
            findNavController().navigate(R.id.action_finalFragment_to_confirmEndMapsFragment)
        }

        bind.concluir.setOnClickListener {
            viewModel.endCall()
            findNavController().navigate(R.id.action_finalFragment_to_menuFragment)
            this.onDestroy()
        }

        return bind.root
    }

}
