
package br.com.seatelecom.app.ui.call.search
/*
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.core.ListFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import br.com.seatelecom.seapoint.MainActivity
import br.com.seatelecom.seapoint.R
import br.com.seatelecom.seapoint.databinding.ItemCallList
import br.com.seatelecom.seapoint.model.call.Call
import br.com.seatelecom.seapoint.ui.login.LoginViewModel
import com.google.android.material.transition.MaterialSharedAxis

class SearchCallFragment(): ListFragment(), AdapterView.OnItemClickListener{

    */
/*private lateinit var user : LoginViewModel
    private lateinit var viewModel: SearchCallViewModel*//*


    //private lateinit var currentCallViewModel: CurrentCallViewModel

    private lateinit var user: LoginViewModel
    private lateinit var viewModel: SearchCallViewModel
    private lateinit var currentProjectViewModel: CurrentProjectViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val forward = MaterialSharedAxis(MaterialSharedAxis.X, true)
        enterTransition = forward

        val backward = MaterialSharedAxis(MaterialSharedAxis.X, false)
        returnTransition = backward
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        user = ViewModelProvider(requireActivity()).get(LoginViewModel::class.java)
        viewModel = ViewModelProvider(requireActivity())[SearchCallViewModel::class.java]
        currentProjectViewModel = ViewModelProvider(requireActivity()).get(CurrentProjectViewModel::class.java)
        val root = inflater.inflate(R.layout.list_fragment, container, false)
        val refresh: SwipeRefreshLayout = root.findViewById(R.id.refresh)

        refresh.setOnRefreshListener {
            viewModel.update()
        }
        viewModel.projects.observe(viewLifecycleOwner, Observer {
            val adapter = Adapter(requireContext(),it)
            listAdapter = adapter
            listView.onItemClickListener = this
            refresh.isRefreshing = false
        })
        val actionBar = (requireActivity() as MainActivity).supportActionBar!!
        actionBar.show()
        actionBar.title = getString(R.string.project_list)
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


    }


    class Adapter(context: Context, val calls: ArrayList<Call>):
        ArrayAdapter<Call>(context, R.layout.item_simple_list,calls) {
        class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val binding: ItemCallList?

            init {
                binding = ItemCallList.bind(itemView)
            }

        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val holder = Holder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_simple_list, parent, false)
            )
            val call = calls.get(position)
            holder.binding!!.protocol.text = call.
            holder.binding!!.address.text = project.description
            return holder.itemView
        }
    }
    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        currentProjectViewModel.setCurrent(viewModel.projects.value!!.get(position))
        val action = SearchPoleFragmentDirections.actionSearchPoleFragmentToCurrentProjectFragment()
        findNavController().navigate(action)
    }


}*/
