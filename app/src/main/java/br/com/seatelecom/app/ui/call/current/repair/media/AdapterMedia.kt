package br.com.seatelecom.app.ui.call.current.repair.media

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import io.realm.RealmList

class AdapterMedia(fragment: Fragment, var files: RealmList<String>) : FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = files.size

    override fun createFragment(position: Int): Fragment {
        return MediaFragment.newInstance(files.get(position)!!, position)
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }
}