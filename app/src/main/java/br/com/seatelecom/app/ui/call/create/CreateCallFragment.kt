package br.com.seatelecom.app.ui.call.create

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.icu.text.SimpleDateFormat
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import br.com.seatelecom.app.R
import br.com.seatelecom.app.databinding.CreateCallUI
import br.com.seatelecom.app.model.CALL_ID
import br.com.seatelecom.app.services.Displacement
import br.com.seatelecom.app.ui.call.current.CallViewModel
import com.google.android.material.snackbar.Snackbar
import java.util.*


class CreateCallFragment : DialogFragment() {


    private lateinit var viewModel: CallViewModel
    lateinit var bind: CreateCallUI
    @SuppressLint("StringFormatInvalid")
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        bind = CreateCallUI.inflate(inflater)

        viewModel = ViewModelProvider(requireActivity())[CallViewModel::class.java]
        viewModel.attendance.observe(viewLifecycleOwner, Observer { start ->
            if(start==null) {
                notification("Protocolo não encontrado")
            } else if(start?.protocol!=""){
                bind.clientName.setTextColor(resources.getColor(R.color.colorPrimaryDisabled))
                bind.group2.visibility = View.VISIBLE
                bind.create.visibility = View.VISIBLE
                bind.clientName.text = start!!.nameClient
                bind.description.text = start!!.description
                bind.operator.text = start!!.operator
                val cal = Calendar.getInstance()
                cal.timeInMillis = start!!.create
                bind.hour.text = SimpleDateFormat("dd/MM/yyyy hh:mm")
                    .format(cal.time)


                val create = Calendar.getInstance()
                create.time.time = start!!.create
//                binding.clientName.text = it.start!!.create

                bind.create.isEnabled = true
            }else{
                notification("Não foi possível encontrar o protocolo!")
            }
        })

        bind.search.setOnClickListener {
            viewModel.getStart(
                bind.searchText.text.toString()
            )
        }
        bind.create.setOnClickListener {
            getPermission()
        }

        val actionBar = (requireActivity() as AppCompatActivity).supportActionBar!!
        actionBar.show()
        actionBar.title = getString(R.string.find_call)

        return bind.root
    }

    fun notification(text: String){
        bind.group2.visibility = View.GONE
        bind.clientName.text = text
        bind.clientName.setTextColor(resources.getColor(R.color.something_wrong))
        bind.create.isEnabled = false
    }

    fun getPermission(){
        try { //Checa as permissões e inicia o escutador da localização.
            if(ActivityCompat.checkSelfPermission(//Verifica se tem permissão
                    requireActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_DENIED
            ) {
                val permissions = arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION
                )

                ActivityCompat.requestPermissions(
                    requireActivity(),
                    permissions, 0
                )

            } else {
                viewModel.create()

                val serviceIntent = Intent(requireContext(), Displacement::class.java)
                serviceIntent.putExtra(CALL_ID,viewModel.call.value!!.id)
                requireActivity().startService(serviceIntent)




                findNavController().navigate(R.id.action_createCallFragment_to_startFragment)
            }

        } catch (ex: Exception) {
            Log.e("ERRO DE LOCALIZAÇÃO", ex.toString())
        }
    }

}