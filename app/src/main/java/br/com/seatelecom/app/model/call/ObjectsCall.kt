package br.com.seatelecom.app.model.call

import android.location.Location
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class Attendance: RealmObject(){
    var create: Long = 0L
    var protocol: String = ""
    var description: String = ""
    var operator: String = ""
    var nameClient: String = ""
    var conclusion: Long = 0L

    fun getJson(): JsonObject{
        val obj = JsonObject()

        obj.addProperty("create",create)
        obj.addProperty("protocol",protocol)
        obj.addProperty("description",description)
        obj.addProperty("operator",operator)
        obj.addProperty("nameClient",nameClient)
        obj.addProperty("conclusion",conclusion)

        return obj
    }

    fun set(attendance: Attendance){
        create = attendance.create
        protocol = attendance.protocol
        description = attendance.description
        operator = attendance.operator
        nameClient = attendance.nameClient
        conclusion = attendance.conclusion
    }


}

open class BeforeRepair(): RealmObject(){

    var startVerify: Long = 0L
    var defectLocation: LocationSeaPoint? = null

    fun getJson(): JsonObject{
        val obj = JsonObject()

        obj.addProperty("startVerify", startVerify)
        if(defectLocation == null){
            obj.add("defectLocation", LocationSeaPoint().getJson())
        } else {
            obj.add("defectLocation", defectLocation!!.getJson())
        }
        return obj
    }
}


open class Repair: RealmObject(){
    var startRepair: Long = 0
    var comments: RealmList<Comment> = RealmList()
    var endRepair: Long = 0

    fun getJson():JsonObject{
        val obj = JsonObject()

        obj.addProperty("endRepair",endRepair)
        obj.addProperty("startRepair",startRepair)
        val comments = JsonArray()
        for (aux in this.comments){
            comments.add(aux.getJson())
        }

        obj.add("comments", comments)

        return obj
    }
}

open class Comment: RealmObject(){
    var create: Long = Calendar.getInstance().timeInMillis
    var text: String = ""
    var files: RealmList<String> = RealmList()
    var filesSync: RealmList<Boolean> = RealmList()


    fun getJson():JsonObject{
        val obj = JsonObject()

        obj.addProperty("create",create)
        obj.addProperty("text",text)
        val filesArray = JsonArray()
        for (aux in files){
            filesArray.add(aux)
        }
        obj.add("files", filesArray)
        val filesArrayBoolean = JsonArray()
        for (aux in filesSync){
            filesArrayBoolean.add(aux)
        }
        obj.add("filesSync", filesArrayBoolean)

        return obj
    }
}

open class

LocationSeaPoint: RealmObject(){
    var local: RealmList<Double> = RealmList()
    var create: Long = Calendar.getInstance().timeInMillis
    fun setLocation(location: Location?){
        local.add(location?.latitude)
        local.add(location?.longitude)
        local.add(location?.altitude)
    }

    fun getJson():JsonObject{
        val obj = JsonObject()

        obj.addProperty("create", create)
        val array = JsonArray()

        for (aux in local){
            array.add(aux)
        }

        obj.add("local", array)

        return obj
    }
}

open class MaterialSelected: RealmObject(){
    var _id: String = ""
    var name: String = ""
    var code: String = ""
    var unity: String = ""
    var category: String = ""
    var companyId: String = ""
    var amount: Double = 0.0

    fun getJson():JsonObject{
        val obj = JsonObject()

//        obj.addProperty("_id", _id)
        obj.addProperty("name", name)
        obj.addProperty("code", code)
        obj.addProperty("unity", unity)
        obj.addProperty("category", category)
        obj.addProperty("amount", amount)

        return obj
    }

    fun set(material: Material){
        _id = material._id
        name = material.name
        code = material.code
        unity = material.unity
        category = material.category
    }

}

open class Material: RealmObject(){

    @PrimaryKey var _id: String = ""
    var name: String = ""
    var code: String = ""
    var unity: String = ""
    var category: String = ""
    var companyId: String = ""


    fun get(): Material {
        val aux = Material()

        aux._id = _id
        aux.name = name
        aux.code = code
        aux.unity = unity
        aux.category = category

        return aux
    }

    override fun toString(): String {
        return "Name: " + name + "\nUnidade: " + unity
    }

    fun equals(other: Material): Boolean {
        return this._id==other._id
    }

}