package br.com.seatelecom.app.ui.call.current.start

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.icu.text.SimpleDateFormat
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import br.com.seatelecom.app.R
import br.com.seatelecom.app.databinding.StartCallUI
import br.com.seatelecom.app.model.CALL_ID
import br.com.seatelecom.app.services.Displacement
import br.com.seatelecom.app.ui.call.current.CallViewModel
import com.google.android.material.transition.MaterialSharedAxis
import java.util.*

class StartFragment : Fragment() {
    private lateinit var viewModel: CallViewModel
    private lateinit var locationManager: LocationManager
    private lateinit var myLocation: GetLocation
    private lateinit var bind: StartCallUI

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(requireActivity())[CallViewModel::class.java]

        val backward = MaterialSharedAxis(MaterialSharedAxis.X, false)
        reenterTransition = backward

        val forward = MaterialSharedAxis(MaterialSharedAxis.X, true)
        exitTransition = forward
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bind = StartCallUI.inflate(inflater)

        viewModel.call.observe(viewLifecycleOwner, Observer {



            bind.protocol.text = it.attendance!!.protocol
            bind.clientName.text = it.attendance!!.nameClient
            bind.description.text = it.attendance!!.description
            bind.operator.text = it.attendance!!.operator
            //(requireActivity() as MainActivity).supportActionBar?.title = it.attendance!!.protocol
            val actionBar = (requireActivity() as AppCompatActivity).supportActionBar!!
            actionBar.show()
            actionBar.title = getString(R.string.title_attendance_1)

            val cal = Calendar.getInstance()
            cal.timeInMillis = it.attendance!!.create
            bind.hour.text = SimpleDateFormat("dd/MM/yyyy hh:mm")
                .format(cal.time)

            if (it.beforeRepair == null) {

                bind.initVerify.isEnabled = true
                bind.defectLocation.isEnabled = false

            } else if ((it.beforeRepair?.startVerify != 0L)
                    .and(it.beforeRepair?.defectLocation == null)
            ) {

                bind.initVerify.isEnabled = false
                bind.defectLocation.isEnabled = true

                bind.startVerify.text = SimpleDateFormat("dd/MM/yyyy hh:mm")
                    .format(it.beforeRepair!!.startVerify)

                bind.starVerifyTitle.visibility = View.VISIBLE
                bind.startVerify.visibility = View.VISIBLE

            } else if (it.beforeRepair?.defectLocation != null) {

                bind.initVerify.isEnabled = false
                bind.defectLocation.isEnabled = false

                bind.startVerify.text = SimpleDateFormat("dd/MM/yyyy hh:mm")
                    .format(it.beforeRepair!!.startVerify)

                bind.starVerifyTitle.visibility = View.VISIBLE
                bind.startVerify.visibility = View.VISIBLE

                bind.startLocation.text = SimpleDateFormat("dd/MM/yyyy hh:mm")
                    .format(it.beforeRepair!!.defectLocation!!.create)


                bind.starLocationTitle.visibility = View.VISIBLE
                bind.startLocation.visibility = View.VISIBLE

                bind.initRepair.setBackgroundResource(R.drawable.custom_button)
                bind.initRepair.isEnabled = true

            }
        })


        bind.initVerify.setOnClickListener {
//            val serviceIntent = Intent(requireContext(), Displacement::class.java)
//            serviceIntent.putExtra(CALL_ID,viewModel.call.value!!.id)
//            requireActivity().startService(serviceIntent)
            viewModel.startVerify()
        }

        bind.defectLocation.setOnClickListener { it: View ->
            bind.defectLocation.isEnabled = false
            getLocation(it)
        }

        bind.initRepair.setOnClickListener {

            findNavController().navigate(R.id.action_startFragment_to_tabFragment)

        }
        return bind.root
    }


    fun getLocation(view: View) {
        try { //Checa as permissões e inicia o escutador da localização.
            if (ActivityCompat.checkSelfPermission(
//Verifica se tá sem permissão
                    requireActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION,
                ) == PackageManager.PERMISSION_DENIED
            ) {
                bind.defectLocation.isEnabled = true
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(
                        Manifest.permission.CAMERA,
                    ), 0
                )  //pede a permissão ao usuário caso esteja negado
            } else {
                locationManager = requireActivity()
                    .getSystemService(Context.LOCATION_SERVICE) as LocationManager

                myLocation = GetLocation()

                locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    5,
                    1f,
                    myLocation
                )
            }

        } catch (ex: Exception) {
            Log.e("ERRO DE LOCALIZAÇÃO", ex.toString())
        }
    }


    inner class GetLocation : LocationListener {
        override fun onLocationChanged(location: Location) {
            viewModel.setDefectLocation(location)
            //viewModel.setLocation(location)
            locationManager.removeUpdates(myLocation)
        }

        override fun onProviderDisabled(provider: String) {

        }

        override fun onProviderEnabled(provider: String) {

        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

        }

    }
}