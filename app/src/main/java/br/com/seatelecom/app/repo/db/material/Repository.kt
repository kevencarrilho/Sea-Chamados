package br.com.seatelecom.app.repo.db.material

import androidx.lifecycle.MutableLiveData
import br.com.seatelecom.auth.data.model.LoggedInUser
import br.com.seatelecom.auth.data.model.Token
import br.com.seatelecom.app.model.IP_SERVER
import br.com.seatelecom.app.core.model.MaterialService
import br.com.seatelecom.app.model.call.Material
import com.google.gson.GsonBuilder
import io.realm.Realm
import io.realm.RealmChangeListener
import io.realm.RealmResults
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Repository() {


    companion object {
        private val retrofit = Retrofit.Builder()
            .baseUrl(IP_SERVER)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()

        private val webService: MaterialService = retrofit.create(
            MaterialService::class.java
        )

        fun get(materialCheck: MutableLiveData<Boolean>? = null): MutableLiveData<ArrayList<Material>> {
            val realm = Realm.getDefaultInstance()
            val materials = MutableLiveData<ArrayList<Material>>()
            val list = ArrayList<Material>()

            val materialList = realm.where(Material::class.java).findAll()

            materialList.addChangeListener { mat, changeSet ->
                for (i in changeSet.insertions) {
                    list.add(mat[i]!!)
                }

            }

            list.addAll(materialList)
            materials.postValue(list)
            if(materialCheck!=null){
                materialCheck.value = true
            }
            return materials
        }

        fun getCategory(
            category: String,
            companyId: String,

        ): ArrayList<Material> {

            val realm = Realm.getDefaultInstance()
            var materials = Realm.getDefaultInstance().where(Material::class.java)
                .equalTo("companyId", companyId)
                .findAll()
            val auxList = ArrayList<Material>()
//            materials.addChangeListener { mat, changeSet ->
//                for (i in changeSet.insertions) {
//                    val s = mat[i]
//                    auxList.add(mat[i]!!)
//                }
//            }

            for (material in materials) {
                val aux = material.get()
                if (aux.category.equals(category)) auxList.add(aux)
            }
            realm.close()
            return auxList
        }
    }

}