package br.com.seatelecom.app.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import org.bson.types.ObjectId

val CHANNEL_ID = "GET_LOCATION"
val IP_SERVER_KEYCLOAK: String = "https://auth.seasolutions.com.br/auth/"
val CALL_ID = "CALL_ID"
val ONGOING_NOTIFICATION_ID = 1
//const val REALM = "sea-point"
// DEPLOY

const val REALM = "sea-point"
//val CLIENT_SECRET = "28fb72c1-522b-47cc-9255-d021e79df0f2"
val IP_SERVER: String = "https://api.seapoint.seasolutions.com.br/"

val CLIENT_SECRET = "91a28dd1-08ea-44f8-aa90-160b7801fe81"
// TESTE
//const val REALM = "sea-point-teste"
//val CLIENT_SECRET = "6bca711e-4cc7-4e15-964d-eb5ff9f57a2b"
//val IP_SERVER: String = "http://172.18.32.150:5500/"


const val URL_TOKEN = "realms/" + REALM + "/protocol/openid-connect/token"
const val URL_LOGOUT = "realms/" + REALM + "/protocol/openid-connect/token"



open class Current(
    var projectId: String = ObjectId().toString(),
    @PrimaryKey var name: String = "current"
): RealmObject()
