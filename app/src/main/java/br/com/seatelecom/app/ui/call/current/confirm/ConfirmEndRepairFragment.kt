package br.com.seatelecom.app.ui.call.current.confirm

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.seatelecom.app.databinding.ListUI
import br.com.seatelecom.app.ui.call.current.CallViewModel
import br.com.seatelecom.app.ui.call.current.repair.AdapterComments

class ConfirmEndRepairFragment : Fragment() {

    private lateinit var viewModel: CallViewModel
    private lateinit var bind: ListUI

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bind = ListUI.inflate(inflater)

        viewModel = ViewModelProvider(requireActivity())[CallViewModel::class.java]


        viewModel.call.observe(viewLifecycleOwner, Observer {

            bind.list.adapter = AdapterComments(it.repair!!.comments, this)
            bind.list.layoutManager = LinearLayoutManager(requireContext(),
                RecyclerView.VERTICAL,false)

        })

        return bind.root
    }

}