package br.com.seatelecom.app.ui.call.current.repair

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import br.com.seatelecom.app.R
import br.com.seatelecom.app.databinding.ItemCommentUI
import br.com.seatelecom.app.model.call.Comment
import br.com.seatelecom.app.ui.call.current.repair.media.AdapterMedia
import io.realm.RealmList

class AdapterComments (var comments: RealmList<Comment>, val fragment: Fragment):
    RecyclerView.Adapter<AdapterComments.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val bind: ItemCommentUI
        init {
            bind = ItemCommentUI.bind(itemView)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_comment,parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val comment = comments.get(position)
        if(comment?.text!=""){
            holder.bind.comment.text = comment?.text
        } else {
            holder.bind.comment.visibility = View.GONE
            holder.bind.title2.setPadding(16,16,16,16)
        }
        if(comment!!.files.size!=0){
            if(comment!!.files.size==1){
                holder.bind.seekBar.visibility = View.GONE
            } else {
                holder.bind.seekBar.max = comment!!.files.size - 1
                if(comment!!.files.size>2){
                    holder.bind.seekBar.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
                }
                holder.bind.seekBar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {
                    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                        holder.bind.viewPager2.currentItem = progress
                    }

                    override fun onStartTrackingTouch(seekBar: SeekBar?) {
                    }

                    override fun onStopTrackingTouch(seekBar: SeekBar?) {
                    }
                })
            }


            holder.bind.viewPager2.adapter = AdapterMedia(fragment,comment!!.files)
            holder.bind.viewPager2.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback(){
                override fun onPageSelected(position: Int) {
                    holder.bind.seekBar.progress = position
                }
            })

        }
    }

    override fun getItemCount(): Int {
        return comments.size
    }
}

