package br.com.seatelecom.app.ui.call.current.repair.comment

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.seatelecom.app.model.call.Comment
import io.realm.RealmList

class AddCommentViewModel: ViewModel() {

    private val _file = MutableLiveData<RealmList<String>>().apply {
        value = RealmList()
    }

    fun addFile(name: String) {
        val file = _file.value
        file?.add(name)
        _file.value = file
    }

    fun removeFile(name: String){
        val file = _file.value
        file?.remove(name)
        _file.value = file
    }

    val comment = Comment()

}