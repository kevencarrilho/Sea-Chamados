package br.com.seatelecom.app.ui.call.current

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import br.com.seatelecom.app.R
import br.com.seatelecom.app.model.CALL_ID
import br.com.seatelecom.app.services.Displacement
import com.google.android.material.transition.MaterialSharedAxis

class CurrentCallFragment: Fragment(){

    private val TAB_TITLES = arrayOf(
        R.string.start_tab,
        R.string.end_tab,
        R.string.information_tab
    )

    private lateinit var viewModel: CallViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)

        viewModel = ViewModelProvider(requireActivity())[CallViewModel::class.java]

        val serviceIntent = Intent(requireContext(), Displacement::class.java)
        serviceIntent.putExtra(CALL_ID,viewModel.call.value?.id)
        requireActivity().startService(serviceIntent)

//        if (viewModel.call.value?.beforeRepair?.defectLocation==null){
//            val action = CurrentCallFragmentDirections.actionCurrentCallFragmentToStartFragment()
//            findNavController().navigate(action)
//
//        } else if ((viewModel.call.value?.beforeRepair?.defectLocation!=null)
//                .and(viewModel.call.value?.repair?.endRepair==0L)) {
//            val action = CurrentCallFragmentDirections.actionCurrentCallFragmentToRepairFragment()
//            findNavController().navigate(action)
//        } else if((viewModel.call.value?.repair?.endRepair!=0L)
//                .and(viewModel.call.value?.end==0L)){
//            val action = CurrentCallFragmentDirections.actionCurrentCallFragmentToMaterialListFragment()
//            findNavController().navigate(action)
//        } else if(viewModel.call.value?.end!=0L){
//            val action = CurrentCallFragmentDirections.actionCurrentCallFragmentToEndFragment()
//            findNavController().navigate(action)
//        } else {
//
//        }


    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val forward = MaterialSharedAxis(MaterialSharedAxis.X, true)
        enterTransition = forward

        val backward = MaterialSharedAxis(MaterialSharedAxis.X, false)
        returnTransition = backward
    }
}