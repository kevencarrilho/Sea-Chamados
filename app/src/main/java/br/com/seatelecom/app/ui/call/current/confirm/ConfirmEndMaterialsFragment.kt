package br.com.seatelecom.app.ui.call.current.confirm

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.seatelecom.app.R
import br.com.seatelecom.app.databinding.ListUI
import br.com.seatelecom.app.model.call.MaterialSelected
import br.com.seatelecom.app.ui.call.current.CallViewModel
import br.com.seatelecom.app.ui.call.current.material.MaterialListFragment
import io.realm.RealmList

class ConfirmEndMaterialsFragment : Fragment() {

    private lateinit var viewModel: CallViewModel
    private lateinit var bind: ListUI

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bind = ListUI.inflate(inflater)

        viewModel = ViewModelProvider(requireActivity())[CallViewModel::class.java]


        viewModel.call.observe(viewLifecycleOwner, Observer {

            bind.list.adapter = MaterialAdapter(it.materialList)
            bind.list.layoutManager = LinearLayoutManager(requireContext(),
                RecyclerView.VERTICAL,false)

        })

        return bind.root
    }

    inner class MaterialAdapter(var list: RealmList<MaterialSelected>) :
        RecyclerView.Adapter<MaterialListFragment.MaterialViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MaterialListFragment.MaterialViewHolder {
            return MaterialListFragment.MaterialViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_material, parent, false)
            )
        }

        override fun getItemCount(): Int {
            return list.size
        }

        override fun onBindViewHolder(modelMaterial: MaterialListFragment.MaterialViewHolder, position: Int) {
            val material = list.get(position)

            modelMaterial.binding.name.text = material!!.name
            modelMaterial.binding.amount.text = "Quantidade: " + material.amount + "\nUnidade: " + material.unity
            modelMaterial.binding.delete.isVisible = false

        }

    }

}